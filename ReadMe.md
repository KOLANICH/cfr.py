[cfr.py] [![Unlicensed work](https://raw.githubusercontent.com/unlicense/unlicense.org/master/static/favicon.png)](https://unlicense.org/)
======
![GitLab Build Status](https://gitlab.com/KOLANICH/cfr.py/badges/master/pipeline.svg)
![GitLab Coverage](https://gitlab.com/KOLANICH/cfr.py/badges/master/coverage.svg)
[![Coveralls Coverage](https://img.shields.io/coveralls/KOLANICH/cfr.py.svg)](https://coveralls.io/r/KOLANICH/cfr.py)
[![Libraries.io Status](https://img.shields.io/librariesio/github/KOLANICH/cfr.py.svg)](https://libraries.io/github/KOLANICH/cfr.py)

Just python bindings for [`CFR`](https://github.com/leibnitz27/cfr) Java decompiler.


